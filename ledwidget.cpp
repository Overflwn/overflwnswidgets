#include "ledwidget.h"
#include <QPainter>

using namespace OverflwnsWidgets;

LedWidget::LedWidget(QWidget* parent)
    :QWidget(parent)
    , m_power(false)
    , m_focalRadius(50)
{
    m_colDark = Qt::darkRed;
    m_colBright = Qt::red;
}

void LedWidget::setColorDark(const QColor& newCol)
{
    m_colDark = newCol;
}

void LedWidget::setColorBright(const QColor& newCol)
{
    m_colBright = newCol;
}

void LedWidget::setPower(bool power)
{
    if(power!=m_power){
        m_power = power;
        emit powerChanged();
        update();
    }
}

void LedWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter ledPainter(this);
    ledPainter.setPen(Qt::black);

    QRadialGradient gradient(this->width()/2, this->height()/2, this->m_focalRadius, this->width()/2 - 15, this->height()/2 - 5);
    gradient.setColorAt(1, m_colBright);
    int r, g, b;
    m_colBright.getRgb(&r, &g, &b);
    if(r <= 245) {
        r += 10;
    }else {
        r = 255;
    }
    if(g <= 245) {
        g += 10;
    }else {
        g = 255;
    }
    if(b <= 245) {
        b += 10;
    }else {
        b = 255;
    }
    gradient.setColorAt(0, QColor::fromRgb(r, g, b));

    if(!m_power) {
        gradient.setColorAt(0, m_colDark);
        gradient.setColorAt(1, Qt::black);
    }

    QBrush brush(gradient);
    ledPainter.setBrush(brush);
    ledPainter.drawRect(rect());
}
