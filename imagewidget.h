#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QGraphicsView>
#include <QLabel>
#include <QWidget>
#include "overflwnsWidgets_global.h"

namespace OverflwnsWidgets {
Q_NAMESPACE

/*enum ImageFormat {
    GRAY8,
    GRAY16,
    RGB24,
};
Q_ENUM_NS(ImageFormat)*/

class OVERFLWNSWIDGETS_EXPORT ImageWidget : public QLabel
{
    Q_OBJECT
    Q_PROPERTY(QImage image MEMBER m_image NOTIFY imageChanged DESIGNABLE true)
    Q_PROPERTY(ImageFormat imageFormat MEMBER m_imageFormat NOTIFY formatChanged DESIGNABLE true)
    Q_PROPERTY(QString test MEMBER m_test NOTIFY testChanged)


public:
    enum ImageFormat {
        GRAY8,
        GRAY16,
        RGB24,
    };
    Q_ENUM(ImageFormat)
    ImageWidget(const ImageWidget&)=delete;
    ImageWidget& operator=(const ImageWidget&)=delete;
    explicit ImageWidget(QWidget* parent=nullptr);
    void setImageData(unsigned char* data, int width, int height, ImageFormat format=GRAY8);

    unsigned char* getImageData() const
    {
        return m_imageData;
    }

    static int getBytesPerPixel(ImageFormat format);
    void unsetImage();
signals:
    void imageChanged(QImage newImage);
    void testChanged(QString newTest);
    void formatChanged(ImageFormat newFormat);
protected:
    void paintEvent(QPaintEvent *event) override;
private:
    unsigned char* m_imageData=nullptr;
    QImage m_image;
    QString m_test;
    ImageFormat m_imageFormat;
};
}

#endif // IMAGEWIDGET_H
