# Overflwn's Qt Widgets

Small (soon to be) collection of useful widgets that I was missing from Qt, like an LED or an image widget.
You can set the color of the LED and the "glow" radius.

The image widget can show raw image data (currently only 8bit grayscale), it's planned to add support to general QImage objects and other types of raw data.
