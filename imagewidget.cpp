#include "imagewidget.h"
#include <QColorSpace>
#include <QPainter>
#include <QTime>

using namespace OverflwnsWidgets;

ImageWidget::ImageWidget(QWidget* parent)
    :QLabel(parent), m_imageData(nullptr), m_imageFormat(ImageFormat::GRAY8)
{
    setScaledContents(false);
}

void ImageWidget::unsetImage()
{
    m_image = QImage();
}

int ImageWidget::getBytesPerPixel(ImageFormat format) {
    switch(format) {
    case GRAY8:
        return 1;
    case GRAY16:
        return 2;
    case RGB24:
        return 3;
    default:
        return 0;
    }
}

void ImageWidget::setImageData(unsigned char* data, int width, int height, ImageFormat format)
{
    bool createnew = false;
    int bpp = ImageWidget::getBytesPerPixel(format);
    if(m_imageData != nullptr)
    {
        if(width != m_image.width() || height != m_image.height())
        {
            free(m_imageData);
            //if(image != nullptr)
            //    delete image;

            m_imageData = nullptr;
            //image = nullptr;
            createnew = true;
        }else
        {
            // Same dimensions, just override data
            memcpy(m_imageData, data, sizeof(unsigned char)*width*height*bpp);
        }


    }else
    {
        createnew = true;
    }

    if(createnew)
    {
        size_t amountData = width*height*bpp;
        m_imageData = (unsigned char*)malloc(sizeof(unsigned char)*amountData);
        memcpy(m_imageData, data, sizeof(unsigned char)*amountData);
        QImage::Format formatQImage;
        switch(format) {
        case GRAY8:
            formatQImage = QImage::Format_Grayscale8;
            break;
        case GRAY16:
            formatQImage = QImage::Format_Grayscale16;
            break;
        case RGB24:
            formatQImage = QImage::Format_RGB888;
            break;
        default:
            formatQImage = QImage::Format_Grayscale8;
        }

        m_image = QImage(m_imageData, width, height, sizeof(unsigned char)*width*bpp, formatQImage);
    }

    update();

    //image = new QImage("featured_channel.png");
}

void ImageWidget::paintEvent(QPaintEvent *event)
{
    //Q_UNUSED(event)
    QLabel::paintEvent(event);
    //QGraphicsView::paintEvent(event);
    QPainter ledPainter(this);



    //QRect target(0, 0, 50, 50);

    /*ledPainter.setPen(Qt::black);
    ledPainter.setBrush(Qt::black);
    ledPainter.drawRect(QRect(pos().x(), pos().y(), width(), height()));*/

    if(m_imageData == nullptr || m_image.isNull()) //|| image == nullptr)
    {
        ledPainter.fillRect(rect(), Qt::black);
    }else
    {
        //image->convertToColorSpace(QColorSpace::SRgb);
        //ledPainter.drawImage(rect(), m_image);
        // Keep aspect ratio
        //ledPainter.fillRect(rect(), Qt::black);
        auto imScaled = m_image.scaled(size(), Qt::KeepAspectRatio);
        auto rectScaled = imScaled.rect();
        auto x= (int)(rect().width() / 2.0 - rectScaled.width() / 2.0);
        auto y = (int)(rect().height() / 2.0 - rectScaled.height() / 2.0);
        if(x < 0)
            x = 0;
        if(y < 0)
            y = 0;
        ledPainter.drawPixmap((int)x, (int)y, rectScaled.width(), rectScaled.height(), QPixmap::fromImage(imScaled));
        //setPixmap(m_pixmap);
        //show();
        //QRect target(0, 0, 1024, 1024);
        //ledPainter.drawImage(target, m_image);
        //ledPainter.drawImage(rect(), imScaled);


        //setPixmap(QPixmap::fromImage(*image));
        //setPixmap(QPixmap("featured_channel.png"));
    }

}
