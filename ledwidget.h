#ifndef LEDWIDGET_H
#define LEDWIDGET_H

#include <QWidget>
#include "overflwnsWidgets_global.h"

namespace OverflwnsWidgets {
class OVERFLWNSWIDGETS_EXPORT LedWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(bool power READ power WRITE setPower NOTIFY powerChanged)
    Q_PROPERTY(QColor colorDark READ colorDark WRITE setColorDark NOTIFY colorDarkChanged)
    Q_PROPERTY(QColor colorBright READ colorBright WRITE setColorBright NOTIFY colorBrightChanged)
    Q_PROPERTY(qreal focalRadius MEMBER m_focalRadius NOTIFY focalRadiusChanged)

    LedWidget(const LedWidget&)=delete;
    LedWidget& operator=(const LedWidget&)=delete;
public:
    explicit LedWidget(QWidget* parent=nullptr);

    virtual ~LedWidget() {}

    bool power() const
    {
        return m_power;
    }

    QColor colorDark() const
    {
        return m_colDark;
    }

    QColor colorBright() const
    {
        return m_colBright;
    }

    void setColorDark(const QColor& newCol);

    void setColorBright(const QColor& newCol);
public slots:
    void setPower(bool power);
signals:
    void powerChanged();
    void colorDarkChanged();
    void colorBrightChanged();
    void focalRadiusChanged();
protected:
    virtual void paintEvent(QPaintEvent *event) override;
private:
    bool m_power;
    QColor m_colDark;
    QColor m_colBright;
    qreal m_focalRadius;
};
}


#endif // LEDWIDGET_H
